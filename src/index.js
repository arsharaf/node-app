'use strict';

const express = require('express');
const pino = require('pino');
const port = process.env.PORT || 3000;
const app = express();
const logger = pino({ level: 'info' });

app.get('/', (req, res) => res.send('Hello Ahmed!'));

//app.listen(port, () => console.log(`App listening on port ${port}!`));
app.listen(port, function(err){
    if (err) console.log("Error in server setup")
    logger.info("Server listening on Port", port);
})